extends Node


func _ready():
    var v1 = Vector3(0, 0, 0)
    var v2 = Vector3(1, 0, 0)
    var v3 = Vector3(2, 0, 0)

    Map.set_cell(v1, 5)
    Map.set_cell(v2, 10)
    Map.set_cell(v3, 15)

    print(Map.get_cell(v1))
    print(Map.get_cell(v2))
    print(Map.get_cell(v3))
