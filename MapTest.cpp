#include <array>

#include <Godot.hpp>
#include <Node.hpp>
#include <Dictionary.hpp>
#include <Vector3.hpp>


// See: <https://github.com/catchorg/Catch2/blob/master/docs/own-main.md>
#define CATCH_CONFIG_RUNNER
#include "catch2/catch.hpp"

int catch2_runner() {
    // Basic implementation.
    // TODO: Enable command line arguments to be supplied etc.
    Catch::Session session;
    return session.run();
}


class Map : public godot::Node {
    GODOT_CLASS(Map, godot::Node);

    private: 
        std::array<int, 100> cells_;

    public: 
        Map() { };
        void _init() { } ;
        void set_cell(godot::Vector3 coord, int number) {
            cells_[coord.x] = number;
        };
        godot::Dictionary get_cell(godot::Vector3 coord) {
            godot::Dictionary something;
            something["position"] = cells_[coord.x];

            return something;
        }
        static void _register_methods() {
            register_method("set_cell", &Map::set_cell);
            register_method("get_cell", &Map::get_cell);
        }
};

extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o) {
    godot::Godot::gdnative_init(o);

    // Note: This could/should probably be placed somewhere else.
    catch2_runner();
};

extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o) {
    godot::Godot::gdnative_terminate(o);
};

extern "C" void GDN_EXPORT godot_nativescript_init(void *handle) {
    godot::Godot::nativescript_init(handle);
    godot::register_tool_class<Map>();
};

extern "C" void GDN_EXPORT godot_gdnative_singleton() {
};


TEST_CASE( "Splines are reticulated", "[splines]" ) {

    REQUIRE( 1 == 1 ); // Verify correct test operation.


    Map map;
    map.set_cell(godot::Vector3(0, 0, 0), 5);

    // Note: In theory this intermediate `result` variable shouldn't be necessary
    //       but haven't been able to get casting to work correctly.
    godot::Variant result = map.get_cell(godot::Vector3(0, 0, 0))["position"];

    REQUIRE( (int) result == 5 ); // Perform an actual Godot-related test.


    // REQUIRE( 0 == 1 ); // Demonstrates failing test.
}
