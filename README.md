## Original README

(If godot-cpp is not automatically downloaded upon cloning this repository, run 
`git submodule add https://github.com/GodotNativeTools/godot-cpp`)

INSTRUCTIONS:
- `git submodule update --init --recursive`
- `cd godot-cpp`
- `scons platform=<your platform> generate_bindings=yes`
- `cd ..`
- `make`

In order to test that the GDNative script library is working correctly, run `godot projects/Node.gd`. 
The resulting output should be a printout of three dictionaries, whose `position` entries are 5, 10, 
and 15. In order to trigger the segfault, run the file `test` that was created with `make`.

# Mac support

On Mac the Godot test can alternatively be run via:

    /<path>/Godot.app/Contents/MacOS/Godot --quit --path ./project/ Node.tscn

# Integrating Catch2 unit test framework

An example of integrating [Catch2 unit test framework](https://github.com/catchorg/Catch2)
is now included.

When run the output should resemble this:

    [...]
    Godot Engine v3.2.1.stable.official - https://godotengine.org
    [...]
    ===============================================================================
    All tests passed (2 assertions in 1 test case)
    [...]

# Additional context

Originally this project was to demonstrate an issue for a post on Reddit but has
now been modified to demonstrate integrating Catch2 unit test framework:

 * Original thread: "[How might one go about getting a GDNative script to work outside of Godot (e.g. - in a Unit Testing framework)?](https://old.reddit.com/r/godot/comments/flo135/how_might_one_go_about_getting_a_gdnative_script/)"

 * Post describing this repository branch: <https://old.reddit.com/r/godot/comments/flo135/how_might_one_go_about_getting_a_gdnative_script/fm1xfrd/>

