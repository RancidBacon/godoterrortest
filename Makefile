#*************************************
# Makefile for Godot library: HexLib #
#*************************************
#********************
# VARS 
###
GODOTCPP_PATH ?= godot-cpp

UNAME := $(shell uname -s)
ifeq ($(UNAME),Darwin)
        PLATFORM := osx
        CXX := clang++
        EXTRA_FLAGS := -O00
else
        PLATFORM := linux
        CXX := g++
        EXTRA_FLAGS := -O3
endif

CXXFLAGS = $(EXTRA_FLAGS) -Wall \
		   -I${GODOTCPP_PATH}/include \
		   -I${GODOTCPP_PATH}/include/core \
		   -I${GODOTCPP_PATH}/include/gen \
		   -I${GODOTCPP_PATH}/godot_headers
SRC = MapTest
SRCDIR = ./
OBJECTS = $(addsuffix .o, $(addprefix $(SRCDIR), $(SRC)))

#********************
# RULES
###
build: $(OBJECTS)
	$(CXX) -o project/libMapTest.so -shared -rdynamic -g $^ -L${GODOTCPP_PATH}/bin -lgodot-cpp.${PLATFORM}.debug.64
	$(CXX) $(CXXFLAGS) -g test.cpp -o test -L${GODOTCPP_PATH}/bin -lgodot-cpp.${PLATFORM}.debug.64

#********************
# Primary Files
###
.SECONDEXPANSION:
$(OBJECTS): $$(addsuffix .cpp, $$*)
	$(CXX) $(CXXFLAGS) -fPIC -o $@ -c $(addsuffix .cpp, $*) -g


#********************
# Other
###
clean: 
	rm -f *.o
	rm -f project/libMapTest.so
